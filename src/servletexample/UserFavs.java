package servletexample;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "UserFavs", urlPatterns={"/UserFavs"})
public class UserFavs extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head></head><body>");
        String color = request.getParameter("color");
        String font = request.getParameter("font");
        String layout = request.getParameter("layout");
        out.println("<h1>your custom page</h1>");
        out.println("<p>Color: " + color + "</p>");
        out.println("<p>Font: " + font + "</p>");
        out.println("<p>Layout: " + layout+ "</p>");
        out.println("</body></html>");
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
